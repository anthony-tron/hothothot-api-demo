<?php

namespace app;

use rotor\Exact;
use rotor\Fallback;
use rotor\Get;

class Schema {
    #[Exact, Get('/')]
    public function index() {
        header('Content-Type: application/json; charset=utf-8');

        $sensors = [
            'interior' => [
                'title' => 'Température intérieur',
                'value' => random_int(-5, 60),
                'unit' => '°C',
            ],
            'exterior' => [
                'title' => 'Température extérieur',
                'value' => random_int(-5, 40),
                'unit' => '°C',
            ],
        ];

        $message = match(true) {
            
            $sensors['exterior']['value'] > 35 => [
                'type' => 'alert',
                'short' => 'Hot Hot Hot !',
                'full' => 'Qui a oublié d\'éteindre le four ?',
            ],

            $sensors['exterior']['value'] < 0 => [
                'type' => 'alert',
                'short' => 'Banquise en vue !',
                'full' => 'Qui a oublié d\'éteindre le four ?',
            ],

            $sensors['interior']['value'] > 50 => [
                'type' => 'alert',
                'short' => 'Appelez les pompiers ou arrêtez votre barbecue !',
                'full' => '',
            ],

            $sensors['interior']['value'] > 22 => [
                'type' => 'alert',
                'short' => 'Baissez le chauffage !',
                'full' => '',
            ],

            $sensors['interior']['value'] < 12 => [
                'type' => 'alert',
                'short' => 'Montez le chauffage ou mettez votre gros pull !',
                'full' => '',
            ],

            $sensors['interior']['value'] < 0 => [
                'type' => 'alert',
                'short' => 'Canalisations gelées, appelez SOS plobmier - et mettez un bonnet !',
                'full' => '',
            ],

            default => null,
            
        };

        echo json_encode([
            'sensors' => $sensors,
            'message' => $message,
        ]);
    }

    #[Fallback]
    public function fallback() {
        http_response_code(404);
    }
}
