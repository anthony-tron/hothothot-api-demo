<?php

namespace rotor;

#[\Attribute]
class Put extends Route {
    public function __construct($route) {
        parent::__construct($route, ['PUT']);
    }
}
