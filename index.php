<?php

require_once 'src/autoload.php';

rotor\Schema::route(
    new app\Schema(),
    $_SERVER['REQUEST_URI'],
    $_SERVER['REQUEST_METHOD']
);
